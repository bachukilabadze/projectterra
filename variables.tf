variable "vpc_cidr_block" {
  type        = string
  description = "VPC IP Range"
}

variable "subnet_cidrs" {
  description = "CIDR subnets"
  type        = list(string)
}


variable "availability_zones" {
  description = "availability zones for subnets"
  type        = list(string)
}

variable "aws_region" {
  description = "AWS region"
  type        = string
}

variable "environments" {
  description = "environments"
  type        = list(string)
}

variable "instance_type" {
  description = "EC2 instance type"
}

variable "key_name" {
  description = "Key Name"
  type        = string
}

