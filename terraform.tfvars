vpc_cidr_block     = "10.0.0.0/16"
subnet_cidrs       = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
availability_zones = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
aws_region         = "eu-central-1"
environments       = ["Production", "Staging", "Dev"]
instance_type      = "t2.micro"
key_name           = "project-key"