provider "aws" {
  region = var.aws_region
}

// --------------------------------------- 1) VPC
resource "aws_vpc" "terravpc" {
  cidr_block       =  var.vpc_cidr_block
  tags = {
    Name = "my vpc"
  }
}
// --------------------------------------- 2) Subnets
resource "aws_subnet" "terrasubnets" {
  vpc_id            = aws_vpc.terravpc.id
  count             = length(var.subnet_cidrs)
  cidr_block        = element(var.subnet_cidrs, count.index)
  availability_zone = element(var.availability_zones, count.index)
  tags = {
    Name = "my subnets"
  }
}

// ---------------------------------------- 3) EC2
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "instances" {
  count                      = length(var.environments)
  subnet_id                   = aws_subnet.terrasubnets[count.index].id     # The VPC subnet ID.
  instance_type               = var.instance_type                           # The type of the Instance.       
  ami                         = data.aws_ami.ubuntu.id                      # The ID of the AMI used to launch the instance.
  key_name                    = var.key_name                                # The key name of the Instance.
  vpc_security_group_ids      = [aws_security_group.victoria_security.id]   # The associated security groups in a non-default VPC.
  associate_public_ip_address = true                                        # Whether or not the Instance is associated with a public IP address or not (Boolean).

  tags = {
    Name = "${var.environments[count.index]} ENV"
    
  }
}


// ---------------------------------------- 3) Security Group
resource "aws_security_group" "victoria_security" {
  vpc_id = aws_vpc.terravpc.id

  dynamic "ingress" {
    for_each = [22, 80, 443]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = [22, 80, 443]
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

// ---------------------------------------- 4) Elastic IPs
// --- Provides an Elastic IP resource.
resource "aws_eip" "elastikuri" {
  count    = length(var.subnet_cidrs)
  instance = aws_instance.instances[count.index].id
}
// --- Provides an AWS EIP Association as a top level resource, to associate and disassociate Elastic IPs from AWS Instances and Network Interfaces.
resource "aws_eip_association" "elastikuri_assoc" {
  count         = length(var.subnet_cidrs)
  allocation_id = aws_eip.elastikuri[count.index].id
  instance_id   = aws_instance.instances[count.index].id
}

// ----------------- CHATGPT?
# Create the aws_internet_gateway resource and associate it with the VPC.
resource "aws_internet_gateway" "my_internet_gateway" {
  vpc_id = aws_vpc.terravpc.id

  tags = {
    Name = "my-internet-gateway"
  }
}
// Create the aws_route_table resource with the VPC ID and add a default route to the Internet Gateway.
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.terravpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_internet_gateway.id
  }

  tags = {
    Name = "public-route-table"
  }
}
// Create the aws_route_table_association resource to associate each subnet with the public route table.
resource "aws_route_table_association" "public_subnet_association" {
  count          = length(var.subnet_cidrs)
  subnet_id      = aws_subnet.terrasubnets[count.index].id
  route_table_id = aws_route_table.public.id
}
